package andriod.jsmith.crystalball;

import java.util.Random;

public class Predictions {

    public static Predictions predictions;
    public String[] answers;

    private Predictions() {
        answers = new String[]{
                "Shake at Sonic Speed.",
                "Gotta Go Fast.",
                "Shake It Fast.",
                "Shake it Faster, Faster Fastafasfaster",
                "You will die in 7 days"
        };
    }

    public static Predictions get() {
        if (predictions == null) {
            predictions = new Predictions();
        }
        return predictions;
    }

    public String getPrediction() {
        int rnd = new Random().nextInt(answers.length);
        return answers[rnd];
    }

}
